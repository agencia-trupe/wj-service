-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: 26-Nov-2015 às 19:36
-- Versão do servidor: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wjservice`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `titulo`, `link`, `created_at`, `updated_at`) VALUES
(1, 0, '20151126183006_exxafood.png', '<p><span style="color:#fcb040;"><em><strong><span style="font-size:50px;">Controle das Refei&ccedil;&otilde;es</span></strong></em></span></p>\r\n\r\n<p><em><span style="font-size:40px;">em Restaurante Corporativo</span></em></p>\r\n', 'produtos/exxa-food', '2015-11-26 20:30:06', '2015-11-26 20:30:06'),
(2, 1, '20151126183024_wjcard.png', '<p><span style="color:#fcb040;"><em><strong><span style="font-size:50px;">Controle das Refei&ccedil;&otilde;es</span></strong></em></span></p>\r\n\r\n<p><em><span style="font-size:40px;">em Restaurante Corporativo</span></em></p>\r\n', 'produtos/wj-card', '2015-11-26 20:30:24', '2015-11-26 20:30:24');

-- --------------------------------------------------------

--
-- Estrutura da tabela `chamadas`
--

CREATE TABLE `chamadas` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `chamadas`
--

INSERT INTO `chamadas` (`id`, `ordem`, `imagem`, `titulo`, `texto`, `link`, `created_at`, `updated_at`) VALUES
(1, 0, '', 'Conciliação', '<p>A gest&atilde;o dos cart&otilde;es tem como objetivo a concilia&ccedil;&atilde;o das transa&ccedil;&otilde;es de cart&otilde;es de cr&eacute;ditos e d&eacute;bitos garantido que se tenha controle de todos os recebimentos realizados pelas administradoras de cart&otilde;es, taxas, etc.</p>\r\n', 'produtos/wj-card', '0000-00-00 00:00:00', '2015-11-26 20:24:34'),
(2, 0, '', 'Chargeback', '<p>Este Servi&ccedil;o de gest&atilde;o tem como objetivo evitar as surpresas em vendas canceladas (chargeback) pelas adquirentes, gerando um controle preciso de todos</p>\r\n', 'produtos/wj-card', '0000-00-00 00:00:00', '2015-11-26 20:25:17'),
(3, 0, '', 'Frente de Caixa', '<p>O sistema <span style="color:#16456f;"><strong><em>Exxafood</em>&nbsp;</strong></span>foi desenvolvido para apresentar agilidade, com as telas de f&aacute;cil entendimento e sem trocas desnecess&aacute;rias. O sistema &eacute; intuitivo ao uso do operador de caixa em sua utiliza&ccedil;&atilde;o, trazendo dinamismo. Realiza as vendas &agrave; vista com as diferentes formas de pagamento, e est&aacute; adequado &agrave;s LEIS FISCAIS.</p>\r\n', 'produtos/exxa-food', '0000-00-00 00:00:00', '2015-11-26 20:26:40'),
(4, 0, '', 'Acesso a Catracas', '<p>O sistema <span style="color:#16456f;"><em><strong>Exxafood</strong></em></span> integra-se com qualquer modelo de catraca, e atrav&eacute;s desta comunica&ccedil;&atilde;o o gesto das refei&ccedil;&otilde;es no restaurante ou a empresa autogest&atilde;o, obter&aacute; total controle da unidade. O sistema realiza a leitura de catracas, grupos de catracas, por hor&aacute;rios, cargos e etc.</p>\r\n', 'produtos/exxa-food', '0000-00-00 00:00:00', '2015-11-26 20:27:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `ordem`, `nome`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 0, 'SBT', '20151126183327_cliente-sbt.jpg', '2015-11-26 20:33:27', '2015-11-26 20:33:27'),
(2, 1, 'Sodexo', '20151126183348_cliente-sodexo.jpg', '2015-11-26 20:33:48', '2015-11-26 20:33:48'),
(3, 2, 'Pandora', '20151126183356_cliente-pandora.jpg', '2015-11-26 20:33:56', '2015-11-26 20:33:56'),
(4, 3, 'Ambev', '20151126183403_clientes-ambev.jpg', '2015-11-26 20:34:03', '2015-11-26 20:34:03'),
(5, 4, 'Xerox', '20151126183409_cliente-xerox.jpg', '2015-11-26 20:34:09', '2015-11-26 20:34:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
`id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `endereco`, `googlemaps`, `facebook`, `created_at`, `updated_at`) VALUES
(1, 'contato@wjconciliacao.com.br', '(11) 3106-5213', '<p>Estrada do Itapeti, 100 | Quadra 18</p><p>Lote 05 Mogi das Cruzes/SP</p><p>CEP 08771-910</p>', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d20118.445336209363!2d-46.118179717393396!3d-23.43574896207004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cdd11dc57ca485%3A0x46b9f484dd896baf!2sEstr.+do+Itapeti%2C+100+-+Luiz+Carlos%2C+Guararema+-+SP%2C+08900-000!5e0!3m2!1spt-BR!2sbr!4v1448462210598" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 'https://www.facebook.com/wjservice', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
`id` int(10) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `institucional`
--

CREATE TABLE `institucional` (
`id` int(10) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `institucional`
--

INSERT INTO `institucional` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<p>Texto Institucional.</p>\r\n', '0000-00-00 00:00:00', '2015-11-26 20:30:51');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_11_25_133156_create_banners_table', 1),
('2015_11_25_133211_create_chamadas_table', 1),
('2015_11_25_133232_create_institucional_table', 1),
('2015_11_25_133248_create_produtos_table', 1),
('2015_11_25_133259_create_clientes_table', 1),
('2015_11_25_133309_create_contato_table', 1),
('2015_11_25_133318_create_contatos_recebidos_table', 1),
('2015_11_25_133328_create_orcamentos_recebidos_table', 1),
('2015_11_25_200608_create_solucoes_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `orcamentos_recebidos`
--

CREATE TABLE `orcamentos_recebidos` (
`id` int(10) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
`id` int(10) unsigned NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `titulo`, `slug`, `texto`, `created_at`, `updated_at`) VALUES
(1, 'Exxa Food', 'exxa-food', '<p>Texto Exxa Food.</p>\r\n', '0000-00-00 00:00:00', '2015-11-26 20:30:59'),
(2, 'WJ Card', 'wj-card', '<p>Texto WJ Card.</p>\r\n', '0000-00-00 00:00:00', '2015-11-26 20:31:06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `solucoes`
--

CREATE TABLE `solucoes` (
`id` int(10) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `solucoes`
--

INSERT INTO `solucoes` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<p>Texto Solu&ccedil;&otilde;es.</p>\r\n', '0000-00-00 00:00:00', '2015-11-26 20:31:13');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$Hkq0xVxt1Y7EWurFtEkibeA4qwkMBIVGuVGSFIhKlIgG07/sj/Rcm', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chamadas`
--
ALTER TABLE `chamadas`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institucional`
--
ALTER TABLE `institucional`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orcamentos_recebidos`
--
ALTER TABLE `orcamentos_recebidos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `solucoes`
--
ALTER TABLE `solucoes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `chamadas`
--
ALTER TABLE `chamadas`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `institucional`
--
ALTER TABLE `institucional`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `orcamentos_recebidos`
--
ALTER TABLE `orcamentos_recebidos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `solucoes`
--
ALTER TABLE `solucoes`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
