(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.lightboxHome = function() {
        var $wrapper = $('#lightbox');
        if (!$wrapper.length) return;

        $wrapper.fadeIn();

        $wrapper.on('click', function(event) {
            if ($(event.target).is('#lightbox')) $(this).fadeOut();
        });
    };

    App.bannersHome = function() {
        var $wrapper = $('.banners');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.banner',
            pagerTemplate: '<a href="#">{{slideNum}}</a>'
        });
    };

    App.scrollContato = function() {
        $('#contato-handle').on('click', function(event) {
            event.preventDefault();

            var target = this.hash;

            $('html, body').stop().animate({
                'scrollTop': $(target).offset().top
            }, 900, 'swing');
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#contato_nome').val(),
                email: $('#contato_email').val(),
                telefone: $('#contato_telefone').val(),
                mensagem: $('#contato_mensagem').val(),
            },
            success: function(data) {
                $form[0].reset();
                $response.hide().text(data.message).fadeIn('slow');
            },
            error: function(data) {
                $response.hide().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.showFormOrcamento = function() {
        $('#orcamento-handle').click(function(event) {
            event.preventDefault();

            $(this).parent().parent().addClass('show');
        });
    };

    App.envioOrcamento = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-orcamento-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/orcamento',
            data: {
                nome: $('#orcamento_nome').val(),
                email: $('#orcamento_email').val(),
                telefone: $('#orcamento_telefone').val(),
                produto: $('#orcamento_produto').val(),
                mensagem: $('#orcamento_mensagem').val(),
            },
            success: function(data) {
                $form[0].reset();
                $response.hide().text(data.message).fadeIn('slow');
            },
            error: function(data) {
                $response.hide().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.init = function() {
        this.lightboxHome();
        this.bannersHome();
        this.scrollContato();
        $('#form-contato').on('submit', this.envioContato);
        $('#form-orcamento').on('submit', this.envioOrcamento);
        this.showFormOrcamento();
    };

    $(document).ready(function() {
        App.init();
        $('input, textarea').placeholder();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
