@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Produto: {{ $produto->titulo }}</h2>
    </legend>

    {!! Form::model($produto, [
        'route'  => ['painel.produtos.update', $produto->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
