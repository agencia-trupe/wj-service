@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Soluções</h2>
    </legend>

    {!! Form::model($solucoes, [
        'route'  => ['painel.solucoes.update', $solucoes->id],
        'method' => 'patch'])
    !!}

    @include('painel.solucoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
