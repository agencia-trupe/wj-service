@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Chamadas
        </h2>
    </legend>

    @if(!count($chamadas))
    <div class="alert alert-warning" role="alert">Nenhum chamada cadastrada.</div>
    @else
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($chamadas as $chamada)
            <tr class="tr-row" id="{{ $chamada->id }}">
                <td>{{ $chamada->titulo }}</td>
                <td class="crud-actions">
                    <a href="{{ route('painel.chamadas.edit', $chamada->id ) }}" class="btn btn-primary btn-sm">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
