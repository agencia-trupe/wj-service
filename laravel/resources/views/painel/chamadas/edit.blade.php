@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Chamada: {{ $chamada->titulo }}</h2>
    </legend>

    {!! Form::model($chamada, [
        'route'  => ['painel.chamadas.update', $chamada->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.chamadas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
