@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Institucional</h2>
    </legend>

    {!! Form::model($institucional, [
        'route'  => ['painel.institucional.update', $institucional->id],
        'method' => 'patch'])
    !!}

    @include('painel.institucional.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
