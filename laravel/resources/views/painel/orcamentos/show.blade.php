@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Orçamentos Recebidos</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $orcamento->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $orcamento->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">{{ $orcamento->email }}</div>
    </div>

    @if($orcamento->telefone)
    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $orcamento->telefone }}</div>
    </div>
    @endif

    <div class="form-group">
        <label>Produto</label>
        <div class="well">{{ $orcamento->produto }}</div>
    </div>

    <div class="form-group">
        <label>Mensagem</label>
        <div class="well">{{ $orcamento->mensagem }}</div>
    </div>

    <a href="{{ route('painel.orcamentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
