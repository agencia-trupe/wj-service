@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>Orçamentos Recebidos</h2>
    </legend>

    @if(!count($orcamentosrecebidos))
    <div class="alert alert-warning" role="alert">Nenhum orçamento recebido.</div>
    @else
    <table class="table table-striped table-bordered table-hover ">
        <thead>
            <tr>
                <th>Data</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($orcamentosrecebidos as $orcamento)

            <tr class="tr-row @if(!$orcamento->lido) warning @endif">
                <td>{{ $orcamento->created_at }}</td>
                <td>{{ $orcamento->nome }}</td>
                <td>{{ $orcamento->email }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.orcamentos.destroy', $orcamento->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.orcamentos.show', $orcamento->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Ler mensagem
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    {!! $orcamentosrecebidos->render() !!}
    @endif

@stop
