@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Clientes
            <a href="{{ route('painel.clientes.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Cliente</a>
        </h2>
    </legend>

    @if(!count($clientes))
    <div class="alert alert-warning" role="alert">Nenhum cliente cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="clientes">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Imagem</th>
                <th>Nome</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($clientes as $cliente)
            <tr class="tr-row" id="{{ $cliente->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td><img src="{{ url('assets/img/clientes/'.$cliente->imagem) }}" alt="" style="width:100%;max-width:180px;height:auto;"></td>
                <td>{{ $cliente->nome }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.clientes.destroy', $cliente->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.clientes.edit', $cliente->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
