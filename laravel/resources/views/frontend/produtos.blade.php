@extends('frontend.common.template')

@section('content')

    <div class="center paginas">
        <div class="paginas-titulo">
            <h1>Produtos</h1>
        </div>

        <div class="paginas-conteudo">
            <div class="menu">
                @foreach($produtos as $menu)
                <a href="{{ route('produtos', $menu->slug) }}" @if($menu->slug == $produto->slug) class="active" @endif>{{ $menu->titulo }}</a>
                @endforeach
            </div>

            <div class="texto">
                <p style="text-align:center;margin-bottom:45px;">
                    <img src="{{ asset('assets/img/produtos/'.$produto->imagem) }}" alt="">
                </p>
                {!! $produto->texto !!}
            </div>
        </div>
    </div>

@endsection
