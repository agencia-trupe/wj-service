@extends('frontend.common.template')

@section('content')

    <div class="banners-wrapper">
        <div class="center">
            <div class="banners">
                @foreach($banners as $banner)
                <div class="banner" style="background-image: url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
                    <div class="texto-wrapper">
                        <div class="texto">
                            {!! $banner->titulo !!}
                            <a href="{{ $banner->link }}">
                                <span class="texto">Quero saber mais</span>
                                <span class="seta"></span>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="cycle-pager"></div>
            </div>
        </div>
    </div>

    <div class="chamadas">
        <div class="center">
            <?php $i = 1; ?>
            @foreach($chamadas as $chamada)
            <a href="{{ $chamada->link }}" class="chamada-{{ $i++ }}">
                <div class="titulo">{{ $chamada->titulo }}</div>
                <div class="imagem"></div>
                <div class="texto">{!! $chamada->texto !!}</div>
            </a>
            @endforeach
        </div>
    </div>

    <div id="lightbox">
        <div class="lightbox-padding">
            <div class="lightbox-bg">
                <p class="titulo">
                    Seja Bem Vindo!
                    <span>Conheça nossos produtos</span>
                </p>

                <a href="{{ route('produtos', 'exxa-food') }}">
                    <img src="{{ asset('assets/img/layout/exxa-food.png') }}" alt="">
                    <div class="botao">
                        <span class="texto">Quero saber mais</span>
                        <span class="seta"></span>
                    </div>
                </a>

                <a href="{{ route('produtos', 'wj-card') }}">
                    <img src="{{ asset('assets/img/layout/wj-card.png') }}" alt="">
                    <div class="botao">
                        <span class="texto">Quero saber mais</span>
                        <span class="seta"></span>
                    </div>
                </a>
            </div>
        </div>
    </div>

@endsection
