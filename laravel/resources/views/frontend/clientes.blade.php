@extends('frontend.common.template')

@section('content')

    <div class="center paginas">
        <div class="paginas-titulo">
            <h1>Clientes</h1>
        </div>

        <div class="paginas-conteudo">
            <div class="texto">
                <div class="clientes">
                    @foreach($clientes as $cliente)
                    <div>
                        <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="{{ $cliente->nome }}" title="{{ $cliente->nome }}">
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection