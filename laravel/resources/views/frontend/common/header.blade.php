    <header @if(Route::currentRouteName() == 'home') class="home" @endif>
        <div class="center">
            <h1><a href="{{ route('home') }}">{{ config('site.name') }}</a></h1>
            <nav>
                <a href="{{ route('institucional') }}" @if(Route::currentRouteName() == 'institucional') class="active" @endif>Institucional</a>
                <a href="{{ route('produtos') }}" @if(Route::currentRouteName() == 'produtos') class="active" @endif>Produtos</a>
                <a href="{{ route('solucoes') }}" @if(Route::currentRouteName() == 'solucoes') class="active" @endif>Soluções</a>
                <a href="{{ route('clientes') }}" @if(Route::currentRouteName() == 'clientes') class="active" @endif>Clientes</a>
                <a href="#orcamento" id="contato-handle">Contato</a>
                @if($contato->facebook)
                <a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a>
                @endif
            </nav>
        </div>
    </header>
