    <footer>
        <div id="orcamento">
            <div class="center">
                <a href="javascript:void(0)" id="orcamento-handle">
                    <span class="texto">Solicite um orçamento</span>
                    <span class="seta"></span>
                </a>

                <form action="" id="form-orcamento" method="POST">
                    <input type="name" name="nome" id="orcamento_nome" placeholder="NOME" required>
                    <input type="email" name="email" id="orcamento_email" placeholder="E-MAIL" required>
                    <input type="name" name="telefone" id="orcamento_telefone" placeholder="TELEFONE">
                    <div class="select-wrapper">
                        <select name="produto" id="orcamento_produto">
                            @foreach($produtos_list as $select)
                            <option value="{{ $select->titulo }}">{{ $select->titulo }}</option>
                            @endforeach
                        </select>
                    </div>
                    <textarea name="mensagem" id="orcamento_mensagem" placeholder="MENSAGEM" required></textarea>
                    <input type="submit" value="Enviar">
                    <div class="response-wrapper">
                        <span id="form-orcamento-response"></span>
                    </div>
                </form>
            </div>
        </div>

        <div id="contato">
            <div class="center">
                <div class="info">
                    <p class="titulo">Fale conosco</p>
                    <p class="telefone">{{ $contato->telefone }}</p>
                    <div class="endereco">{!! $contato->endereco !!}</div>
                </div>

                <form action="" id="form-contato" method="POST">
                    <input type="name" name="nome" id="contato_nome" placeholder="NOME" required>
                    <input type="email" name="email" id="contato_email" placeholder="E-MAIL" required>
                    <input type="name" name="telefone" id="contato_telefone" placeholder="TELEFONE">
                    <textarea name="mensagem" id="contato_mensagem" placeholder="MENSAGEM" required></textarea>
                    <input type="submit" value="Enviar">
                    <div class="response-wrapper">
                        <span id="form-contato-response"></span>
                    </div>
                </form>
            </div>
        </div>
        <div class="googlemaps">{!! $contato->googlemaps !!}</div>
        <div class="copyright">
            <div class="center">
                <p>© {{ date('Y') }} {{ config('site.name') }} · Todos os direitos reservados.</p>
                <p><a href="http://trupe.net" target="_blank">Criação de sites</a>: <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a></p>
            </div>
        </div>
    </footer>
