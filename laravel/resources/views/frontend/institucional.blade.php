@extends('frontend.common.template')

@section('content')

    <div class="center paginas">
        <div class="paginas-titulo">
            <h1>Institucional</h1>
        </div>

        <div class="paginas-conteudo">
            <div class="texto">
                {!! $institucional->texto !!}
            </div>
        </div>
    </div>

@endsection