@extends('frontend.common.template')

@section('content')

    <div class="center paginas">
        <div class="paginas-titulo">
            <h1>Soluções</h1>
        </div>

        <div class="paginas-conteudo">
            <div class="texto">
                {!! $solucoes->texto !!}
            </div>
        </div>
    </div>

@endsection