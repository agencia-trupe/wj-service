<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Solucoes extends Model
{
    protected $table = 'solucoes';

    protected $guarded = ['id'];
}
