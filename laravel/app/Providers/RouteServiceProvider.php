<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('banners', 'App\Models\Banner');
        $router->model('chamadas', 'App\Models\Chamada');
        $router->model('institucional', 'App\Models\Institucional');
        $router->model('produtos', 'App\Models\Produto');
        $router->model('solucoes', 'App\Models\Solucoes');
        $router->model('clientes', 'App\Models\Cliente');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('orcamentos', 'App\Models\OrcamentoRecebido');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('produto_slug', function($value) {
            return \App\Models\Produto::whereSlug($value)->first() ?: \App::abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
