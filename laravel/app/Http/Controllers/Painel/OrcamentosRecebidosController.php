<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\OrcamentoRecebido;

class OrcamentosRecebidosController extends Controller
{
    public function index()
    {
        $orcamentosrecebidos = OrcamentoRecebido::orderBy('id', 'DESC')->paginate(15);

        return view('painel.orcamentos.index', compact('orcamentosrecebidos'));
    }

    public function show(OrcamentoRecebido $orcamento)
    {
        $orcamento->update(['lido' => 1]);

        return view('painel.orcamentos.show', compact('orcamento'));
    }

    public function destroy(OrcamentoRecebido $orcamento)
    {
        try {

            $orcamento->delete();
            return redirect()->route('painel.orcamentos.index')->with('success', 'Orçamento excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir orçamento: '.$e->getMessage()]);

        }
    }
}
