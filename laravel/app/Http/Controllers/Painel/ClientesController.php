<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClientesRequest;
use App\Http\Controllers\Controller;

use App\Models\Cliente;
use App\Helpers\CropImage;

class ClientesController extends Controller
{
    private $image_config = [
        'width'  => 190,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/clientes/'
    ];

    public function index()
    {
        $clientes = Cliente::ordenados()->get();

        return view('painel.clientes.index', compact('clientes'));
    }

    public function create()
    {
        return view('painel.clientes.create');
    }

    public function store(ClientesRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Cliente::create($input);
            return redirect()->route('painel.clientes.index')->with('success', 'Cliente adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar cliente: '.$e->getMessage()]);

        }
    }

    public function edit(Cliente $cliente)
    {
        return view('painel.clientes.edit', compact('cliente'));
    }

    public function update(ClientesRequest $request, Cliente $cliente)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $cliente->update($input);
            return redirect()->route('painel.clientes.index')->with('success', 'Cliente alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar cliente: '.$e->getMessage()]);

        }
    }

    public function destroy(Cliente $cliente)
    {
        try {

            $cliente->delete();
            return redirect()->route('painel.clientes.index')->with('success', 'Cliente excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir cliente: '.$e->getMessage()]);

        }
    }
}
