<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests\SolucoesRequest;
use App\Http\Controllers\Controller;
use App\Models\Solucoes;

class SolucoesController extends Controller
{
    public function index()
    {
        $solucoes = Solucoes::first();

        return view('painel.solucoes.index', compact('solucoes'));
    }

    public function update(SolucoesRequest $request, Solucoes $solucoes)
    {
        try {

            $input = $request->all();

            $solucoes->update($input);
            return redirect()->route('painel.solucoes.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }
}
