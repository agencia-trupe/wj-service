<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ChamadasRequest;
use App\Http\Controllers\Controller;

use App\Models\Chamada;
use App\Helpers\CropImage;

class ChamadasController extends Controller
{
    public function index()
    {
        $chamadas = Chamada::all();

        return view('painel.chamadas.index', compact('chamadas'));
    }

    public function edit(Chamada $chamada)
    {
        return view('painel.chamadas.edit', compact('chamada'));
    }

    public function update(ChamadasRequest $request, Chamada $chamada)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            $chamada->update($input);
            return redirect()->route('painel.chamadas.index')->with('success', 'Chamada alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar chamada: '.$e->getMessage()]);

        }
    }
}
