<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;

use App\Helpers\CropImage;

class ProdutosController extends Controller
{
    private $image_config = [
        'width'  => null,
        'height' => null,
        'path'   => 'assets/img/produtos/'
    ];

    public function index()
    {
        $produtos = Produto::all();

        return view('painel.produtos.index', compact('produtos'));
    }

    public function create()
    {
        return view('painel.produtos.create');
    }

    public function store(ProdutosRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Produto::create($input);
            return redirect()->route('painel.produtos.index')->with('success', 'Produto adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar produto: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto)
    {
        return view('painel.produtos.edit', compact('produto'));
    }

    public function update(ProdutosRequest $request, Produto $produto)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $produto->update($input);
            return redirect()->route('painel.produtos.index')->with('success', 'Produto alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar produto: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto)
    {
        try {

            $produto->delete();
            return redirect()->route('painel.produtos.index')->with('success', 'Produto excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir produto: '.$e->getMessage()]);

        }
    }
}
