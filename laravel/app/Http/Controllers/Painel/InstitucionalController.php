<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests\InstitucionalRequest;
use App\Http\Controllers\Controller;
use App\Models\Institucional;

class InstitucionalController extends Controller
{
    public function index()
    {
        $institucional = Institucional::first();

        return view('painel.institucional.index', compact('institucional'));
    }

    public function update(InstitucionalRequest $request, Institucional $institucional)
    {
        try {

            $input = $request->all();

            $institucional->update($input);
            return redirect()->route('painel.institucional.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }
}
