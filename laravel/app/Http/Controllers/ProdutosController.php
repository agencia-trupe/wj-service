<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Produto;

class ProdutosController extends Controller
{
    public function index(Produto $produto)
    {
        if (! $produto->exists) $produto = Produto::first() ?: \App::abort('404');

        $produtos = Produto::select('slug', 'titulo')->get();

        return view('frontend.produtos', compact('produtos', 'produto'));
    }
}
