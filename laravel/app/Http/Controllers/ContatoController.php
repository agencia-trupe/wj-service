<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Http\Requests\OrcamentosRecebidosRequest;
use App\Http\Controllers\Controller;

use App\Models\ContatoRecebido;
use App\Models\OrcamentoRecebido;

class ContatoController extends Controller
{
    public function contato(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $contatoRecebido->create($request->all());

        $contato = \App\Models\Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->from(config('site.email'), config('site.name'))
                        ->subject('[CONTATO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => 'Mensagem enviada com sucesso!'
        ];

        return response()->json($response);
    }

    public function orcamento(OrcamentosRecebidosRequest $request, OrcamentoRecebido $orcamentoRecebido)
    {
        $orcamentoRecebido->create($request->all());

        $contato = \App\Models\Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.orcamento', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->from(config('site.email'), config('site.name'))
                        ->subject('[ORÇAMENTO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => 'Orçamento enviado com sucesso!'
        ];

        return response()->json($response);
    }
}
