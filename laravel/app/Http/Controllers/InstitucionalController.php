<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Institucional;

class InstitucionalController extends Controller
{
    public function index()
    {
        $institucional = Institucional::first();

        return view('frontend.institucional', compact('institucional'));
    }
}
