<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Solucoes;

class SolucoesController extends Controller
{
    public function index()
    {
        $solucoes = Solucoes::first();

        return view('frontend.solucoes', compact('solucoes'));
    }
}
