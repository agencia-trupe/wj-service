<?php

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('institucional', ['as' => 'institucional', 'uses' => 'InstitucionalController@index']);
Route::get('produtos/{produto_slug?}', ['as' => 'produtos', 'uses' => 'ProdutosController@index']);
Route::get('solucoes', ['as' => 'solucoes', 'uses' => 'SolucoesController@index']);
Route::get('clientes', ['as' => 'clientes', 'uses' => 'ClientesController@index']);
Route::post('contato', ['as' => 'contato', 'uses' => 'ContatoController@contato']);
Route::post('orcamento', ['as' => 'orcamento', 'uses' => 'ContatoController@orcamento']);


// Painel
Route::group(['prefix' => 'painel', 'namespace' => 'Painel', 'middleware' => 'auth'], function() {
    Route::get('/', ['as' => 'painel', 'uses' => 'HomeController@index']);
    Route::resource('banners', 'BannersController');
    Route::resource('chamadas', 'ChamadasController');
    Route::resource('institucional', 'InstitucionalController');
    Route::resource('produtos', 'ProdutosController');
    Route::resource('solucoes', 'SolucoesController');
    Route::resource('clientes', 'ClientesController');
    Route::resource('contato/recebidos', 'ContatosRecebidosController');
    Route::resource('contato', 'ContatoController');
    Route::resource('orcamentos', 'OrcamentosRecebidosController');
    Route::resource('usuarios', 'UsuariosController');
    Route::post('ckeditor-upload', 'HomeController@imageUpload');
    Route::post('order', 'HomeController@order');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


// Auth
Route::group(['prefix' => 'painel', 'namespace' => 'Auth'], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'auth', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
});
