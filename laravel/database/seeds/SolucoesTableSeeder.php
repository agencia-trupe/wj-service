<?php

use Illuminate\Database\Seeder;

class SolucoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('solucoes')->insert([
            'texto' => 'solucoes'
        ]);
    }
}
