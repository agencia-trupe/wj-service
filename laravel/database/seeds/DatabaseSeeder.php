<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(InstitucionalTableSeeder::class);
        $this->call(ChamadasTableSeeder::class);
        $this->call(ProdutosTableSeeder::class);
        $this->call(SolucoesTableSeeder::class);

        Model::reguard();
    }
}
