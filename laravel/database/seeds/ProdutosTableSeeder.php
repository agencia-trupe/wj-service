<?php

use Illuminate\Database\Seeder;

class ProdutosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produtos')->insert([
            [
                'titulo' => 'Exxa Food',
                'slug'   => 'exxa-food'
            ],
            [
                'titulo' => 'WJ Card',
                'slug'   => 'wj-card'
            ]
        ]);
    }
}
