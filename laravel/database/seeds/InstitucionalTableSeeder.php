<?php

use Illuminate\Database\Seeder;

class InstitucionalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('institucional')->insert([
            'texto' => 'institucional'
        ]);
    }
}
