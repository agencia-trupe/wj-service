<?php

use Illuminate\Database\Seeder;

class ChamadasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chamadas')->insert([
            ['titulo' => 'Conciliação'],
            ['titulo' => 'Chargeback'],
            ['titulo' => 'Frente de Caixa'],
            ['titulo' => 'Acesso a Catracas']
        ]);
    }
}
