<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email'      => 'contato@wjconciliacao.com.br',
            'telefone'   => '(11) 3106-5213',
            'endereco'   => '<p>Estrada do Itapeti, 100 | Quadra 18</p><p>Lote 05 Mogi das Cruzes/SP</p><p>CEP 08771-910</p>',
            'googlemaps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d20118.445336209363!2d-46.118179717393396!3d-23.43574896207004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cdd11dc57ca485%3A0x46b9f484dd896baf!2sEstr.+do+Itapeti%2C+100+-+Luiz+Carlos%2C+Guararema+-+SP%2C+08900-000!5e0!3m2!1spt-BR!2sbr!4v1448462210598" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
            'facebook'   => 'https://www.facebook.com/wjservice',
        ]);
    }
}
